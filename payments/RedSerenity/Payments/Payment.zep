namespace RedSerenity\Payments;

class Payment implements Payment\PaymentInterface {

	protected _CreditCard      = null;
	protected _ShippingAddress = null;
	protected _BillingAddress  = null;

	public function __construct(<Payment\CreditCard> CreditCard, <Payment\Address> BillingAddress, <Payment\Address> ShippingAddress) {
		let this->_CreditCard      = CreditCard;
		let this->_BillingAddress  = BillingAddress;
		let this->_ShippingAddress = ShippingAddress;
	}

	public function CreditCard(<Payment\CreditCard> CreditCard = null) -> null|<Payment\CreditCard> {
		if (CreditCard) {
			let this->_CreditCard = CreditCard;
		}

		return this->_CreditCard;
	}

	public function ShippingAddress(<Payment\Address> ShippingAddress = null) -> null|<Payment\Address> {
		if (ShippingAddress) {
			let this->_ShippingAddress = ShippingAddress;
		}

		return this->_ShippingAddress;
	}

	public function BillingAddress(<Payment\Address> BillingAddress = null) -> null|<Payment\Address> {
		if (BillingAddress) {
			let this->_BillingAddress = BillingAddress;
		}

		return this->_BillingAddress;
	}

}