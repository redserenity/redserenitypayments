namespace RedSerenity\Payments\Payment;

class BankAccount implements BankAccountInterface {

	const TYPE_PERSONAL_CHECKING = "PERSONAL_CHECKING";
	const TYPE_PERSONAL_SAVINGS  = "PERSONAL_SAVINGS";
	const TYPE_BUSINESS_CHECKING = "BUSINESS_CHECKING";
	const TYPE_BUSINESS_SAVINGS  = "BUSINESS_SAVINGS";

	protected _AccountNumber = null;
	protected _RoutingNumber = null;

	protected _Type = null;

	public function __construct(int AccountNumber=null, int RoutingNumber=null, string Type=null) {
		this->AccountNumber(AccountNumber);
		this->RoutingNumber(RoutingNumber);
		this->Type(Type);
	}

	public function AccountNumber(int AccountNumber = null) -> null|int {
		if (AccountNumber) {
			let this->_AccountNumber = AccountNumber;
		}

		return this->_AccountNumber;
	}

	public function RoutingNumber(int RoutingNumber = null) -> null|int {
		if (RoutingNumber) {
			let this->_RoutingNumber = RoutingNumber;
		}

		return this->_RoutingNumber;
	}

	public function Type(string Type = null) -> null|string {
		if (Type) {
			switch (this->_Type) {
				case self::TYPE_PERSONAL_CHECKING:
				case self::TYPE_PERSONAL_SAVINGS:
				case self::TYPE_BUSINESS_CHECKING:
				case self::TYPE_BUSINESS_SAVINGS:
					break;
				default:
					throw new InvalidBankTypeException("Invalid bank type (" . Type . ") specified.");
			}
			let this->_Type = Type;
		}

		return this->_Type;
	}

	public function TypeName() -> string {
		switch (this->_Type) {
			case self::TYPE_PERSONAL_CHECKING: return "Personal Checking";
			case self::TYPE_PERSONAL_SAVINGS:  return "Personal Savings";
			case self::TYPE_BUSINESS_CHECKING: return "Business Checking";
			case self::TYPE_BUSINESS_SAVINGS:  return "Business Savings";
		}

		return "Invalid Type";
	}

	public function IsValid() -> bool {
		return (this->_AccountNumber != null && this->_RoutingNumber != null && this->_Type != null);
	}

}