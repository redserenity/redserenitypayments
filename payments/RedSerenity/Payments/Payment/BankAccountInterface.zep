namespace RedSerenity\Payments\Payment;

interface BankAccountInterface {

	public function AccountNumber(int AccountNumber = null) -> null|int;
	public function RoutingNumber(int RoutingNumber = null) -> null|int;
	public function Type(string Type = null) -> null|string;
	public function IsValid() -> bool;

}