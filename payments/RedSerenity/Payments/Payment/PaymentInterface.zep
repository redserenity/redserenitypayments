namespace RedSerenity\Payments\Payment;

interface PaymentInterface {

	public function CreditCard(<CreditCard> CreditCard = null) -> null|<CreditCard>;
	public function ShippingAddress(<Address> ShippingAddress = null) -> null|<Address>;
	public function BillingAddress(<Address> BillingAddress = null) -> null|<Address>;

}