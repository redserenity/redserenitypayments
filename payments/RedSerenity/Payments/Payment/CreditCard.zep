namespace RedSerenity\Payments\Payment;

class CreditCard {

	const TYPE_VISA        = "VISA";
	const TYPE_MASTERCARD  = "MASTERCARD";
	const TYPE_AMEX        = "AMEX";
	const TYPE_DISCOVER    = "DISCOVER";
	const TYPE_DINERS_CLUB = "DINERS_CLUB";
	const TYPE_JCB         = "JCB";

	const RGX_CARD_VISA       = "/^4[0-9]{0,}$/";
	const RGX_CARD_VPRECA     = "/^428485[0-9]{0,}$/";
  const RGX_CARD_POSTEPAY   =  "/^(402360|402361|403035|417631|529948){0,}$/";
  const RGX_CARD_CARTASI    = "/^(432917|432930|453998)[0-9]{0,}$/";
  const RGX_CARD_ENTROPAY   = "/^(406742|410162|431380|459061|533844|522093)[0-9]{0,}$/";
  const RGX_CARD_O2MONEY    = "/^(422793|475743)[0-9]{0,}$/";
	const RGX_CARD_MASTERCARD = "/^(5[1-5]|222[1-9]|22[3-9]|2[3-6]|27[01]|2720)[0-9]{0,}$/";
  const RGX_CARD_MAESTRO    = "/^(5[06789]|6)[0-9]{0,}$/";
  const RGX_CARD_KUKURUZA   = "/^525477[0-9]{0,}$/";
  const RGX_CARD_YUNACARD   = "/^541275[0-9]{0,}$/";
	const RGX_CARD_AMEX       = "/^3[47][0-9]{0,}$/";
	const RGX_CARD_DINERS     = "/^3(?:0[0-59]{1}|[689])[0-9]{0,}$/";
	const RGX_CARD_DISCOVER   = "/^(6011|65|64[4-9]|62212[6-9]|6221[3-9]|622[2-8]|6229[01]|62292[0-5])[0-9]{0,}$/";
	const RGX_CARD_JCB        = "/^(?:2131|1800|35)[0-9]{0,}$/";


	protected _Number;
	protected _Cvv;
	protected _ExpirationMonth;
 	protected _ExpirationYear;
	protected _Type;
	protected _Address;

	public function __construct(string Number = null, string Cvv = null, int ExpMonth = null, int ExpYear = null, <Address> BillingAddress) {

	}

	public function Number(string Number = null) {
		if (Number) {
			let Number = preg_replace("/\D/", "", Number); //"
			if (!self::Validate(Number)) {
				throw new InvalidCreditCardException("Invalid credit card number. (" . Number . ")");
			}

			if (preg_match(self::RGX_CARD_JCB, Number)) { let this->_Type = "JCB"; }
			if (preg_match(self::RGX_CARD_AMEX, Number)) { let this->_Type = "American Express"; }
			if (preg_match(self::RGX_CARD_DINERS, Number)) { let this->_Type = "Diners Club"; }

			if (preg_match(self::RGX_CARD_VPRECA, Number)) { let this->_Type = "V-Preca Visa Prepaid"; }
			if (preg_match(self::RGX_CARD_ENTROPAY, Number)) { let this->_Type = "Entropay Visa"; }
			/*if (preg_match(self::RGX_CARD_POSTEPAY, Number)) { let this->_Type = "Postepay"; }
			if (preg_match(self::RGX_CARD_CARTASI, Number)) { let this->_Type = "CartaSi"; }
			if (preg_match(self::RGX_CARD_O2MONEY, Number)) { let this->_Type = "O2 Money"; }
			if (preg_match(self::RGX_CARD_KUKURUZA, Number)) { let this->_Type = "OkChanger Kukuruza"; }
			if (preg_match(self::RGX_CARD_YUNACARD, Number)) { let this->_Type = "Yuna To Go"; }*/

			if (preg_match(self::RGX_CARD_VISA, Number)) { let this->_Type = "Visa"; }
			if (preg_match(self::RGX_CARD_MASTERCARD, Number)) { let this->_Type = "Mastercard"; }
			if (preg_match(self::RGX_CARD_DISCOVER, Number)) { let this->_Type = "Discover"; }
			if (preg_match(self::RGX_CARD_MAESTRO, Number)) {
				if (true) {
					let this->_Type = "Mastercard";
				} else {
					let this->_Type = "Maestro";
				}
			}

			if (empty this->_Type) {
				let this->_Type = "Unknown";
			}

			let this->_Number = Number;
		}

		return this->_Number;
	}

	public function Cvv(string Cvv = null) -> null|string {
		if (Cvv) {
			let this->_Cvv = Cvv;
		}

		return this->_Cvv;
	}

	public function Expiration(int Month, int Year) -> null|array {
		if (Month && Year) {
			if (Month < 1 || Month > 12) {
				throw new InvalidExpirationException("Invalid expiration month (" . Month . ") specified. Valid months are 1 - 12.");
			}

			if (Year < (int) date("Y")) {
				throw new InvalidExpirationException("Invalid expiration year (" . Month . ") specified. Must be equal or greater than current year and must be 4 digits.");
			}

			if (Year == (int) date("Y") && Month < date("j")) {
				throw new InvalidExpirationException("Expiration date must be in the future. (" . Month . "/" . Year . ") is in the past.");
			}

			let this->_ExpirationMonth = Month;
			let this->_ExpirationYear  = Year;
		}

		return [this->_ExpirationMonth, this->_ExpirationYear];
	}

	public function Type() {
		return this->_Type;
	}

	public function Address(<Address> BillingAddress = null) -> null|<Address> {
		if (BillingAddress) {
			let this->_Address = BillingAddress;
		}

		return this->_Address;
	}

	public function IsValid() -> bool {

	}

	static public function Validate(string Number) -> bool {
		var Key, Value, TmpStr = "";

		for Key, Value in array_reverse(str_split(Number)) {
			let TmpStr = TmpStr + (Key % 2 ? Value * 2 : Value);
		}

		return (array_sum(str_split(TmpStr)) % 10 === 0);
	}



}