namespace RedSerenity\Payments\Payment;

abstract class Address {

	protected FirstName { get, set };
	protected MiddleName { get, set };
	protected LastName { get, set };

	protected StreetAddress { get, set };
	protected SuiteAddress { get, set };
	protected City { get, set };
	protected State { get, set };
	protected Zip { get, set };
	protected ZipFour { get, set };

	protected Phone { get, set };
	protected Email { get, set };

}